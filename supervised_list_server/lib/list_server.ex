defmodule ListServer do
  use GenServer

  ### Public API
  def start_link(list_data_pid) do
    :gen_server.start_link({:local, :list}, __MODULE__, list_data_pid, [])
  end

  def clear() do
    GenServer.cast(:list, :clear)
  end

  def add(item) do
    GenServer.cast(:list, {:add, item})
  end

  def remove(item) do
    GenServer.cast(:list, {:remove, item})
  end

  def items do
    GenServer.call(:list, :items)
  end

  def crash do
    GenServer.cast(:list, :crash)
  end

  ### GenServer API

  def init(list_data_pid) do
    list = Agent.get(list_data_pid, fn state -> state end)
    {:ok, {list, list_data_pid}}
  end

  def handle_cast(:clear, {list, list_data_pid}) do
    {:noreply, {[], list_data_pid}}
  end

  def handle_cast({:add, item}, {list, list_data_pid}) do
    {:noreply, {[item | list], list_data_pid}}
  end

  def handle_cast({:remove, item}, {list, list_data_pid}) do
    {:noreply, {list -- [item], list_data_pid}}
  end

  def handle_call(:items, _from, state = {list, list_data_pid}) do
    {:reply, list, state}
  end

  def handle_cast(:crash, list) do
    1 = 2
  end

  def terminate(_reason, {list, list_data_pid}) do
    Agent.update(list_data_pid, fn _state ->
      list
    end)
  end
end
