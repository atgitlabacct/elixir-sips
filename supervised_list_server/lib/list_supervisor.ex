defmodule ListSupervisor do
  use Supervisor

  def start_link do
    # Start supervisor with empty children
    result = {:ok, sup} = Supervisor.start_link(__MODULE__, [])

    # Add children to supervisor
    start_workers(sup)

    result
  end

  def start_workers(sup) do
    {:ok, list_data_pid} = Supervisor.start_child(sup, worker(Agent, [fn -> [] end]))

    Supervisor.start_child(sup, worker(ListSupSupervisor, [list_data_pid]))
  end

  @impl true
  def init(_list) do
    supervise([], strategy: :one_for_one)
  end
end
